##############################################################################
#                                                                            #
#   BACKUP-SCRIPT                                                            #
#                                                                            #
#   Author: Simon-Pierre Brazeau                                             #
#   Date:   September 1, 2017                                                #
#                                                                            #
#   This script creates a backup tarball of specific directories located     #
#   in the user's Home directory at a given destination. Here are the        #
#   options available:                                                       #
#                                                                            #
#       -r         (Rice) Export files from a rice text file. The rice       #
#                  file contains a file name on every line (view rice.txt    #
#                  for example). Files listed in the rice file should be     #
#                  located in the user's Home directory as well.             #
#                                                                            #
#       -m         (Music) Export Music/ directory.                          #
#                                                                            #
#       -p         (Pictures) Export Pictures/ directory.                    #
#                                                                            #
#       -d         (Documents) Export Documents/ directory.                  #
#                                                                            #
#       -v         (Videos) Export Videos/ directory.                        #
#                                                                            #
#       -a         (All) Export every file listed above in seperate          #
#                  packages.                                                 #
#                                                                            #
#       -A         (All) Export every file listed above in a single          #
#                  package with the name of RMPDV.                           #
#                                                                            #
#       -h        (Home) Export user's Home directory.                       #
#                                                                            #
#       -H [name]  Replace host name to given string.                        #
#                                                                            #
#       -D [dir]   Set destination directory. (default is Home)              #
#                                                                            #
#       -R [file]  Set rice file to read.                                    #
#                                                                            #
##############################################################################

# Flags for exporting
rice=0
music=0
photo=0
docs=0
video=0
allsep=0
allinc=0
hdir=0
chost=0
crice=0

# Final hostname for backup file name
chostname=""

# Directories
dest="$HOME/"
musicdir="Music/"
photodir="Pictures/"
docsdir="Documents/"
videodir="Videos/"
pdir=$(pwd)
sdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
defaultrice="$sdir/rice.txt"
ricefile=""

# Beginning of tar command for exporting
tarcmd="tar -zcf "

###############################
# ======== FUCNTIONS ======== #
###############################
exportMusic () {
  fname=$(python $sdir/weeknum-gen.py $musicname)
  cd $HOME

  # Generate name of directory to export
  fcmd="$tarcmd $dest$fname $musicdir"

  echo "Exporting Music to $dest$fname..."
  eval $fcmd
  echo "Finished exporting Music!"
  cd $pdir
}

exportPhoto () {
  fname=$(python $sdir/weeknum-gen.py $photoname)
  cd $HOME

  # Generate name of directory to export
  fcmd="$tarcmd $dest$fname $photodir"

  echo "Exporting Photos to $dest$fname..."
  eval $fcmd
  echo "Finished exporting Photos!"
  cd $pdir
}

exportDocs () {
  fname=$(python $sdir/weeknum-gen.py $docsname)
  cd $HOME

  # Generate name of directory to export
  fcmd="$tarcmd $dest$fname $docsdir"

  echo "Exporting Documents to $dest$fname..."
  eval $fcmd
  echo "Finished exporting Documents!"
  cd $pdir
}

exportVideo () {
  fname=$(python $sdir/weeknum-gen.py $videoname)
  cd $HOME

  # Generate name of directory to export
  fcmd="$tarcmd $dest$fname $videodir"

  echo "Exporting Videos to $dest$fname..."
  eval $fcmd
  echo "Finished exporting Videos!"
  cd $pdir
}

exportHome () {
  fname=$(python $sdir/weeknum-gen.py $HOME)
  cd $HOME

  # Generate name of directory to export
  fcmd="$tarcmd $dest$fname $homename"

  echo "Exporting Home to $dest$fname..."
  eval $fcmd
  echo "Finished exporting Home!"
  cd $pdir
}

exportAllInc () {
  fname=$(python $sdir/weeknum-gen.py $allincname)
  cd $HOME

  # Generate name of directory to export
  fcmd="$tarcmd $dest$fname $musicdir $photodir $docsdir $videodir"

  echo "Exporting main home directories to $dest$fname..."
  eval $fcmd
  echo "Finished exporting main home directories!"
  cd $pdir
}

exportRice () {
  fname=$(python $sdir/weeknum-gen.py $ricename)
  cd $HOME

  # Generate string with files to export from "rice.txt"
  files=""
  while IFS= read line
  do
    # Add file to string
    files="$files $line"
  done <"$ricefile"

  # Generate name of directory to export
  fcmd="$tarcmd $dest$fname $files"

  echo "Exporting Rice to $dest$fname..."
  eval $fcmd
  echo "Finished exporting Rice!"
  cd $pdir
}

exportAllSep () {
  exportRice
  exportMusic
  exportPhoto
  exportDocs
  exportVideo
}

OPTIND=1    # Reset in case getopts has been used previously

# Go through options
while getopts "rmpdvAahH:D:R:" opt; do
  case "$opt" in
    r)
      rice=1
      ;;
    m)
      music=1
      ;;
    d)
      docs=1
      ;;
    p)
      photo=1
      ;;
    v)
      video=1
      ;;
    A)
      allinc=1
      ;;
    a)
      allsep=1
      ;;
    h)
      hdir=1
      ;;
    H)
      chost=1
      chostname=$OPTARG
      ;;
    D)
      dest=$OPTARG
      ;;
    R)
      crice=1
      ricefile=$OPTARG
      ;;
  esac
done

# Check if custom host has been given
if [ $chost -eq 0 ]
then
  chostname=$(hostname)
fi

# Check if a custon rice file has been given
if [ $crice -eq 0 ]
then
  ricefile=$defaultrice
fi

# Destination names
musicname="-$chostname-Music.tar.gz"
photoname="-$chostname-Photos.tar.gz"
docsname="-$chostname-Documents.tar.gz"
videoname="-$chostname-Videos.tar.gz"
ricename="-$chostname-Rice.tar.gz"
allincname="-$chostname-RMPDV.tar.gz"
homename="-$chostname-Home.tar.gz"

# Export stuff
if [ $rice -eq 1 ]; then exportRice; fi
if [ $music -eq 1 ]; then exportMusic; fi
if [ $photo -eq 1 ]; then exportPhoto; fi
if [ $docs -eq 1 ]; then exportDocs; fi
if [ $video -eq 1 ]; then exportVideo; fi
if [ $allinc -eq 1 ]; then exportAllInc; fi
if [ $allsep -eq 1 ]; then exportAllSep; fi
if [ $hdir -eq 1 ]; then exportHome; fi

exit
